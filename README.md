# haraka-plugin-auth-psql

Checks given user id/email and password with entries in target PostgreSQL database.

## Install
This plugin requires dovecot for doveadm to be installed on the haraka server
```
cd /my/haraka/config/dir
npm install git+https://implab.de/open/haraka-plugin-auth-psql#1.0.0
```

### Enable

Add the following line to the `config/plugins` file.

`haraka-plugin-auth-psql`

## Config

The `auth.psql.json` file has the following structure (defaults shown). Also note that this file will need to be created, if not present, in the `config` directory.

```javascript
{
  "dbuser": "mailu",
  "database": "mailu",
  "password": "",
  "host": "127.0.0.1",
  "port": 5432,
  "max": 100,
  "idleTimeoutMillis": 30000,
  "methods": "PLAIN,LOGIN",
  "trustedSubnets": "20.20.0.0/16"
}
```
