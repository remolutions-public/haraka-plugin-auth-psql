const cfgLoader = require('./src/cfg-loader');
const pgConnect = require('./src/pg-connect');
const capabilities = require('./src/capabilities');

exports.register = function() {
  const plugin = this;
  plugin.logdebug('Initializing auth postgresql plugin.');
  cfgLoader.load_config(plugin);
  pgConnect.connect(plugin);
}

exports.hook_capabilities = function(next, connection) {
  const plugin = this;
  capabilities.hook_capabilities(next, connection, plugin);
}

exports.hook_unrecognized_command = function(next, connection, params) {
  const plugin = this;
  capabilities.hook_unrecognized_command(next, connection, params, plugin);
}

exports.shutdown = function() {
  const plugin = this;
  plugin.loginfo("Shutting down auth plugin.");
  plugin.pool.end();
};
