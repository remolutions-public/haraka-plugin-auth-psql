const harakaConstants = require('haraka-constants');
const constants = require('./constants').constants;
const auth = require('./auth');

exports.hook_capabilities = function(next, connection, plugin) {

  // TODO: TESTING
  // console.info(connection.remote, plugin.cfg.filterSubnets);

  // don't allow AUTH unless private IP, encrypted or trusted subnet
  if (!connection.remote.is_private && !connection.tls.enabled) {
    let isTrustedSubnet = false;
    for (const subnet of plugin.cfg.filterSubnets) {
      const pattern = new RegExp('^' + subnet, 'i');
      if (pattern.test(connection.remote.ip)) {
        isTrustedSubnet = true;
        break;
      }
    }

    // // TESTING
    // console.info({
    //   isTrustedSubnet
    // });

    if (!isTrustedSubnet) {
      connection.logdebug(plugin, 'Auth disabled for insecure public connection');
      return next();
    }
  }

  let methods = [];
  if (plugin.cfg && plugin.cfg.methods) {
    plugin.cfg.methods.split(',').forEach(method => {
      methods.push(method.toUpperCase());
    });
  }

  if (methods.length) {
    connection.capabilities.push(`${constants.AUTH_COMMAND} ${methods.join(' ')}`);
    connection.notes.allowed_auth_methods = methods;
  }

  next();
}


exports.hook_unrecognized_command = function(next, connection, params, plugin) {
  if (params[0].toUpperCase() === constants.AUTH_COMMAND && params[1]) {
    return this.select_auth_method(next, connection, params.slice(1).join(' '), plugin);
  }

  if (!connection.notes.authenticating) return next();

  const am = connection.notes.auth_method;
  if (am === constants.AUTH_METHOD_LOGIN) return auth.auth_login(next, connection, params, plugin);
  if (am === constants.AUTH_METHOD_PLAIN) return auth.auth_plain(next, connection, params, plugin);

  next();
}


exports.select_auth_method = function(next, connection, method, plugin) {
  const split = method.split(/\s+/);
  method = split.shift().toUpperCase();

  if (!connection.notes.allowed_auth_methods) return next();
  if (!connection.notes.allowed_auth_methods.includes(method)) return next();

  if (connection.notes.authenticating) return next(harakaConstants.DENYDISCONNECT, 'bad protocol');

  connection.notes.authenticating = true;
  connection.notes.auth_method = method;

  if (method === constants.AUTH_METHOD_PLAIN) return auth.auth_plain(next, connection, split, plugin);
  if (method === constants.AUTH_METHOD_LOGIN) return auth.auth_login(next, connection, split, plugin);
}
