const pg = require('pg');

exports.connect = (plugin) => {
  plugin.pool = new pg.Pool(plugin.cfg.dbConfig);
  plugin.pool.on('error', function(err, client) {
    plugin.logerror('Idle client error. Probably a network issue or a database restart.' +
      err.message + err.stack);
  });
}
