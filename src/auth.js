const utils = require('haraka-utils');
const crypto = require('crypto');
const harakaConstants = require('haraka-constants');
const constants = require('./constants').constants;
const {
  spawn
} = require('child_process');


exports.check_credentials = async function(next, connection, credentials, method, plugin) {
  connection.notes.authenticating = false;
  if (!(credentials[0] && credentials[1])) {
    connection.respond(504, 'Invalid AUTH string', () => {
      connection.reset_transaction(() => next(harakaConstants.OK));
    });
    return;
  }

  const isValid = await this.verify_password(credentials, method, plugin);
  if (isValid) {
    connection.relaying = true;
    connection.results.add({
      name: 'relay'
    }, {
      pass: plugin.name
    });
    connection.results.add(plugin, {
      pass: method
    });
    connection.results.add({
      name: 'auth'
    }, {
      pass: plugin.name,
      method,
      user: credentials[0],
    });

    connection.respond(235, '2.7.0 Authentication successful', () => {
      connection.authheader = "(authenticated bits=0)\n";
      connection.auth_results(`auth=pass (${method.toLowerCase()})`);
      connection.notes.auth_user = credentials[0];
      if (!plugin.blankout_password) connection.notes.auth_passwd = credentials[1];
      return next(harakaConstants.OK);
    });
    return;
  }

  if (!connection.notes.auth_fails) {
    connection.notes.auth_fails = 0;
  }
  connection.notes.auth_fails++;
  connection.results.add({
    name: 'auth'
  }, {
    fail: `${plugin.name}/${method}`,
  });

  let delay = Math.pow(2, connection.notes.auth_fails - 1);
  if (!(plugin == undefined) && plugin.timeout && delay >= plugin.timeout) {
    delay = plugin.timeout - 1;
  } else if (delay == undefined) {
    delay = 10;
  }
  connection.lognotice(plugin, `delaying for ${delay} seconds`);
  // here we include the username, as shown in RFC 5451 example
  connection.auth_results(`auth=fail (${method.toLowerCase()}) smtp.auth=${credentials[0]}`);
  setTimeout(() => {
    connection.respond(535, '5.7.8 Authentication failed', () => {
      connection.reset_transaction(() => next(harakaConstants.OK));
    });
  }, delay * 1000);
}


exports.verify_password = async function(credentials, method, plugin) {
  return new Promise((resolve) => {
    let user = this.sanitize_input(credentials[0]);
    credentials[0] = user;
    const pw = credentials[1];

    if (user && user.length && pw && pw.length) {
      plugin.pool.connect(function(conErr, client, done) {
        if (conErr) {
          plugin.logerror('Error fetching client from pool. ' + conErr);
          return resolve(false);
        }

        client.query('SELECT password FROM public.user WHERE email=LOWER($1)', [user], function(err, result) {
          //Release the client back to the pool by calling the done() callback.
          done();

          if (err) {
            plugin.logerror('Error running query. ' + err);
            return resolve(false);
          }

          if (result.rows && result.rows.length) {
            const dbPw = result.rows[0]['password'];
            if (dbPw && dbPw.length) {
              const child = spawn('doveadm', [
                'pw', '-t', dbPw, '-p', pw
              ]);

              child.stdout.on('data', data => {
                const verifyResult = data.toString();
                plugin.logdebug('stdout', verifyResult);
                if (verifyResult.indexOf('(verified)') > -1) {
                  resolve(true);
                } else {
                  resolve(false);
                }
              });

              child.stderr.on('data', (data) => {
                plugin.logdebug(`stderr: ${data}`);
                resolve(false);
              });
            } else {
              return resolve(false);
            }
          } else {
            return resolve(false);
          }
        });
      });
    } else {
      resolve(false);
    }
  });
}


exports.sanitize_input = function(str) {
  if (str && str.length) {
    return str.replace(/[\0\x08\x09\x1a\n\r"'\\\%]/g, function(char) {
      switch (char) {
        case "\0":
          return "\\0";
        case "\x08":
          return "\\b";
        case "\x09":
          return "\\t";
        case "\x1a":
          return "\\z";
        case "\n":
          return "\\n";
        case "\r":
          return "\\r";
        case "\"":
        case "'":
        case "\\":
        case "%":
          return "\\" + char; // prepends a backslash to backslash, percent,
          // and double/single quotes
        default:
          return char;
      }
    });
  }
}


exports.auth_plain = function(next, connection, params, plugin) {
  if (params[0]) {
    const credentials = utils.unbase64(params[0]).split(/\0/);
    credentials.shift(); // Discard authid
    return this.check_credentials(next, connection, credentials, constants.AUTH_METHOD_PLAIN, plugin);
  } else {
    if (connection.notes.auth_plain_asked_login) {
      return next(harakaConstants.DENYDISCONNECT, 'bad protocol');
    } else {
      connection.respond(334, ' ', () => {
        connection.notes.auth_plain_asked_login = true;
        return next(harakaConstants.OK);
      });
    }
  }
}


exports.auth_login = function(next, connection, params, plugin) {
  if ((!connection.notes.auth_login_asked_login && params[0]) ||
    (connection.notes.auth_login_asked_login &&
      !connection.notes.auth_login_userlogin)) {
    if (!params[0]) return next(harakaConstants.DENYDISCONNECT, 'bad protocol');

    const login = utils.unbase64(params[0]);
    connection.respond(334, constants.LOGIN_PASSWORD_STRING, () => {
      connection.notes.auth_login_userlogin = login;
      connection.notes.auth_login_asked_login = true;
      return next(harakaConstants.OK);
    });
    return;
  }

  if (connection.notes.auth_login_userlogin) {
    const credentials = [
      connection.notes.auth_login_userlogin,
      utils.unbase64(params[0])
    ];
    connection.notes.auth_login_userlogin = null;
    connection.notes.auth_login_asked_login = false;
    return this.check_credentials(next, connection, credentials, constants.AUTH_METHOD_LOGIN, plugin);
  } else {
    connection.respond(334, constants.LOGIN_USERNAME_STRING, () => {
      connection.notes.auth_login_asked_login = true;
      return next(harakaConstants.OK);
    });
  }
}
