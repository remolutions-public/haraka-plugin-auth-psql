exports.constants = {
  AUTH_COMMAND: 'AUTH',
  AUTH_METHOD_PLAIN: 'PLAIN',
  AUTH_METHOD_LOGIN: 'LOGIN',

  LOGIN_USERNAME_STRING: 'VXNlcm5hbWU6', // Username: base64 coded
  LOGIN_PASSWORD_STRING: 'UGFzc3dvcmQ6', // Password: base64 coded
};
