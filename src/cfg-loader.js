exports.load_config = (plugin) => {
  plugin.cfg = plugin.config.get('auth.psql.json', () => {
    plugin.load_config();
  });

  plugin.cfg.dbConfig = {
    user: plugin.cfg.dbuser,
    database: plugin.cfg.database,
    password: plugin.cfg.password,
    host: plugin.cfg.host,
    port: plugin.cfg.port,
    max: plugin.cfg.max,
    idleTimeoutMillis: plugin.cfg.idleTimeoutMillis
  };

  if (plugin.cfg.trustedSubnets && plugin.cfg.trustedSubnets.length) {
    const subnets = plugin.cfg.trustedSubnets.split(',');
    const subnetOctets = [];
    subnets.forEach(subnet => {
      if (subnet.indexOf('/') > -1) {
        const subsplit = subnet.split('/');
        const ip = subsplit[0].split('.');
        switch (subsplit[1]) {
          case '24':
            subnetOctets.push(ip[0] + '.' + ip[1] + '.' + ip[2] + '.');
            break;
          case '16':
            subnetOctets.push(ip[0] + '.' + ip[1] + '.');
            break;
          case '8':
            subnetOctets.push(ip[0] + '.');
            break;
          default:
            subnetOctets.push(subnet);
        }
      } else {
        subnetOctets.push(subnet);
      }
    });
    plugin.cfg.filterSubnets = subnetOctets;
  }
}
